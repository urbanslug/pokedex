# Pokedex

An assistant to help you know which battles to fight, which gyms to go to, what's worth throwing your pokeball at and much more :P

Sometimes life feels like pokemon!
So many pokemon (things) that you want to catch (achieve) so little time and with the limited power of your pokeball(attention/willpower/energy).
So many pokemon, gyms, friends and so forth garnering for your attention.

You know know what helps you keep your eye on the prize? A pokedex xD!


## Inspired by:
  - logic
  - philosophy
  - psychology
  - [How To Find Your Most Productive Hours](https://blog.trello.com/find-productive-hours)

## LICENSE
GPLv3


Hope I don't get into trademark trouble here.

[How To Find Your Most Productive Hours]: https://blog.trello.com/find-productive-hours
